package structs

type User struct {
	Id   string `json:"id"`
	Name string `json:"name"`
}

type Todo struct {
	Id     string `json:"id"`
	Body   string `json:"body"`
	Done   bool   `json:"done"`
	UserId string `json:"user_id"`
}

type TodoRegister struct {
	Body   string `json:"body"`
	UserId string `json:"user_id"`
}

type Result struct {
	User  User   `json:"user"`
	Todos []Todo `json:"todos"`
}
