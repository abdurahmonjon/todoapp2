package services_grpc

import (
	"api-gateway/structs"
	"api-gateway/todopb"
	"api-gateway/userpb"
	"context"
	"encoding/json"
	"google.golang.org/grpc/credentials/insecure"
	"io"
	"log"
	"time"

	"google.golang.org/grpc"
)

func NewUserServiceGRPCClient(url string) userServiceGRPCClient {
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*3)
	defer cancel()

	conn, er := grpc.DialContext(
		ctx,
		url,
		grpc.WithTransportCredentials(insecure.NewCredentials()),
		grpc.WithBlock(),
	)
	if er != nil {
		return userServiceGRPCClient{}
	}

	client := userpb.NewUserServiceClient(conn)

	return userServiceGRPCClient{
		client: client,
	}
}

type userServiceGRPCClient struct {
	client userpb.UserServiceClient
}

func NewTodoServiceGRPCClient(url string) todoServiceGRPCClient {
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*3)
	defer cancel()

	conn, er := grpc.DialContext(
		ctx,
		url,
		grpc.WithTransportCredentials(insecure.NewCredentials()),
		grpc.WithBlock(),
	)
	if er != nil {
		return todoServiceGRPCClient{}
	}

	client := todopb.NewTodoServiceClient(conn)

	return todoServiceGRPCClient{
		client: client,
	}
}

type todoServiceGRPCClient struct {
	client todopb.TodoServiceClient
}

func (t todoServiceGRPCClient) GetTodosByUserId(ctx context.Context, userId string) ([]structs.Todo, error) {
	ts, er := t.client.GetTodoByUserId(ctx, &todopb.UserIdRequest{
		Id: userId,
	})
	if er != nil {
		return nil, er
	}

	return againstProtoTodos(ts), nil
}

func againstProtoTodo(todo *todopb.Todo) structs.Todo {
	return structs.Todo{
		Id:     todo.Id,
		Body:   todo.Body,
		Done:   todo.Done,
		UserId: todo.UserId,
	}
}

func againstProtoTodos(ts *todopb.Todos) []structs.Todo {
	todos := make([]structs.Todo, 0, len(ts.List))
	for _, t := range ts.List {
		todos = append(todos, againstProtoTodo(t))
	}
	return todos
}

func (t todoServiceGRPCClient) GetTodo(ctx context.Context, todoId string) (structs.Todo, error) {
	todo, er := t.client.GetTodo(ctx, &todopb.TodoIdRequest{
		Id: todoId,
	})

	if er != nil {
		return structs.Todo{}, er
	}

	return structs.Todo{
		Id:     todo.Id,
		Body:   todo.Body,
		Done:   todo.Done,
		UserId: todo.UserId,
	}, nil
}

func (t todoServiceGRPCClient) RegisterTodo(ctx context.Context, rBody io.Reader) (string, error) {
	newTodo := todopb.NewTodo{}

	if er := json.NewDecoder(rBody).Decode(&newTodo); er != nil {
		return "", er
	}

	tId, er := t.client.CreateTodo(ctx, &newTodo)
	if er != nil {
		return "", er
	}
	return tId.GetId(), nil
}

func (t todoServiceGRPCClient) UpdateTodo(ctx context.Context, rBody io.Reader) (string, error) {
	todo := todopb.Todo{}
	if er := json.NewDecoder(rBody).Decode(&todo); er != nil {
		log.Println("4: ", er)
		return "", er
	}
	tId, er := t.client.UpdateTodo(ctx, &todo)
	if er != nil {
		log.Println("3: ", er)
		return "", er
	}
	return tId.GetId(), nil
}

func (c userServiceGRPCClient) GetUserById(ctx context.Context, userId string) (structs.User, error) {
	resp, er := c.client.GetUser(ctx, &userpb.UserIdRequest{
		Id: userId,
	})

	if er != nil {
		return structs.User{}, er
	}

	return structs.User{
		Id:   resp.GetId(),
		Name: resp.GetName(),
	}, nil
}

func (c userServiceGRPCClient) UpdateUser(ctx context.Context, userId, userName string) (string, error) {
	resp, er := c.client.UpdateUser(ctx, &userpb.User{
		Id:   userId,
		Name: userName,
	})

	if er != nil {
		log.Println("11: ", er)
		return "", er
	}

	return resp.GetName(), nil
}

func (c userServiceGRPCClient) RegisterUser(ctx context.Context, name string) (string, error) {
	resp, er := c.client.CreateUser(ctx, &userpb.UserNameRequest{
		Name: name,
	})

	if er != nil {
		return "", er
	}

	return resp.GetId(), nil
}
