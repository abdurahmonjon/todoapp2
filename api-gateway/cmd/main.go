package main

import (
	"api-gateway/handler"
	"api-gateway/services"
	"fmt"
	"github.com/go-chi/chi/v5"
	"github.com/go-chi/chi/v5/middleware"
	"net/http"
)

const (
	userServiceGRPCURL = "localhost:9009"
	todoServiceGRPCURL = "localhost:9008"
)

func main() {
	h := handler.New(services.NewServices(userServiceGRPCURL, todoServiceGRPCURL))

	r := chi.NewRouter()

	r.Use(middleware.Logger)

	r.Post("/user/{name}", h.RegisterUser)               // done
	r.Get("/user/{user-id}", h.GetUser)                  // done
	r.Put("/user/update/{user-id}/{name}", h.UpdateUser) // done

	r.Put("/todo/update", h.UpdateTodo) // done
	r.Post("/todo", h.RegisterTodo)     // done
	r.Get("/todo/{todo-id}", h.GetTodo) // done
	r.Get("/user/{user-id}/todos", h.GetUserTodos)

	fmt.Println("Listening at :9090")
	http.ListenAndServe(":9090", r)
}
