package psql

import (
	"context"
	"database/sql"
	"errors"
	"log"
	"todo-service/todo"
)

type Postgres struct {
	db *sql.DB
}

func New(db *sql.DB) Postgres {
	return Postgres{
		db: db,
	}
}

type PsqlRepo interface {
	InsertTodo(ctx context.Context, todo todo.Todo) error
	DeleteTodo(ctx context.Context, todoId string) error
	UpdateTodo(ctx context.Context, todo todo.Todo) (string, error)
	GetTodosByUser(ctx context.Context, userId string) ([]todo.Todo, error)
	GetTodo(ctx context.Context, todoId string) (todo.Todo, error)
}

func (p Postgres) GetTodo(ctx context.Context, todoId string) (todo.Todo, error) {
	t := todo.Todo{}
	if er := p.db.QueryRowContext(ctx, `select * from todos where id = $1`, todoId).Scan(&t.Id, &t.Body, &t.Done, &t.UserId); er != nil {
		return todo.Todo{}, er
	}
	return t, nil
}

func (p Postgres) UpdateTodo(ctx context.Context, todo todo.Todo) (string, error) {
	userId := ""
	if er := p.db.QueryRowContext(ctx, `select user_id from todos where id = $1`, todo.Id).Scan(&userId); er != nil {
		log.Println("9: ", er)
		return "", er
	}

	if todo.UserId == userId {
		query := `update todos set `

		if todo.Body != "" {
			query += "body = $1 "

			if todo.Done {
				query += ", done = $2 where id = $3"

				if _, er := p.db.ExecContext(ctx, query, todo.Body, todo.Done, todo.Id); er != nil {
					return "", er
				}
				return todo.Id, nil
			}
			query += "where id = $2"
			if _, er := p.db.ExecContext(ctx, query, todo.Body, todo.Id); er != nil {
				log.Println("10: ", er)
				return "", er
			}
			return todo.Id, nil
		}
	}
	return "", errors.New("could not find todo")
}

func (p Postgres) InsertTodo(ctx context.Context, todo todo.Todo) error {
	if _, er := p.db.ExecContext(ctx, `insert into todos values ($1, $2, $3, $4)`, todo.Id, todo.Body, todo.Done, todo.UserId); er != nil {
		return er
	}
	return nil
}

func (p Postgres) DeleteTodo(ctx context.Context, todoId string) error {
	if _, er := p.db.ExecContext(ctx, `delete from todos where id = $1`, todoId); er != nil {
		return er
	}
	return nil
}

func (p Postgres) GetTodosByUser(ctx context.Context, userId string) ([]todo.Todo, error) {
	rows, er := p.db.QueryContext(ctx, `select * from todos where user_id = $1`, userId)
	if er != nil {
		return nil, er
	}
	var ts []todo.Todo
	for rows.Next() {
		t := todo.Todo{}
		if er = rows.Scan(&t.Id, &t.Body, &t.Done, &t.UserId); er != nil {
			return nil, er
		}
		ts = append(ts, t)
	}
	return ts, nil
}
