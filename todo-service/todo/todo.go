package todo

import "github.com/google/uuid"

type Todo struct {
	Id     string `json:"id"`
	Body   string `json:"body"`
	Done   bool   `json:"done"`
	UserId string `json:"user_id"`
}

func New(body, userId string) Todo {
	return Todo{
		Id:     uuid.NewString(),
		Body:   body,
		Done:   false,
		UserId: userId,
	}
}
