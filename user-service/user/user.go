package user

import "github.com/google/uuid"

type User struct {
	Id   string `json:"id"`
	Name string `json:"name"`
}

func New(name string) User {
	return User{
		Id:   uuid.NewString(),
		Name: name,
	}
}
