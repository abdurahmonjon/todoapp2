package grpc_server

import (
	"context"
	"user-service/service"
	"user-service/userpb"
)

func NewGRPCServer(svc service.Service) *GRPCServer {
	return &GRPCServer{
		svc: svc,
	}
}

type GRPCServer struct {
	userpb.UnimplementedUserServiceServer
	svc service.Service
}

func (g *GRPCServer) UpdateUser(ctx context.Context, u *userpb.User) (*userpb.UserNameResponse, error) {
	if er := g.svc.UpdateUser(ctx, u.Id, u.Name); er != nil {
		return nil, er
	}

	return &userpb.UserNameResponse{
		Name: u.Name,
	}, nil
}

func (g *GRPCServer) GetUser(ctx context.Context, req *userpb.UserIdRequest) (*userpb.User, error) {
	u, er := g.svc.GetUser(ctx, req.GetId())
	if er != nil {
		return nil, er
	}

	return &userpb.User{
		Id:   u.Id,
		Name: u.Name,
	}, nil
}

func (g *GRPCServer) CreateUser(ctx context.Context, req *userpb.UserNameRequest) (*userpb.UserIdResponse, error) {
	id, er := g.svc.CreateUser(ctx, req.GetName())
	if er != nil {
		return nil, er
	}

	return &userpb.UserIdResponse{
		Id: id,
	}, nil
}
